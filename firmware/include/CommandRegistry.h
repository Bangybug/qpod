#ifndef _COMMAND_REGISTRY_H_
#define _COMMAND_REGISTRY_H_

#include "SerialCommand.h"

void command_servo_set_pulse();

extern const SerialCommandFunction commandRegistry[];

#endif
