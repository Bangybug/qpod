#ifndef SERIAL_H_
#define SERIAL_H_

#define RX_BUF_SIZE 16
#define TX_BUF_SIZE 16

typedef struct {
	char incoming[RX_BUF_SIZE];
	char outgoing[TX_BUF_SIZE];
	unsigned short rxi;
	volatile unsigned char eol;
} Serial;


void serial_init(Serial *p);
void serial_init_gpio();
void serial_init_usart();
void serial_init_dma();

void serial_send_dma(const char *buff);
const char *serial_as_string();

#endif
