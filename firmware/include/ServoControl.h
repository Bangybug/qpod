#ifndef SERVOCONTROL_H_
#define SERVOCONTROL_H_

#include "stm32f10x.h"

enum
{
	SERVO_SHORTEST_PULSE = 50,
	SERVO_LONGEST_PULSE = 249
};

typedef struct {
	volatile uint16_t pulses[12];
} ServoControl;

void servocontrol_init(ServoControl *p);
void servocontrol_init_gpio();
void servocontrol_init_timers();


#endif
