#ifndef SERIALCOMMAND_H_
#define SERIALCOMMAND_H_

typedef struct
{
	Serial *pSerial;
	unsigned short rxi;
	unsigned short cmdSize;
	unsigned int err;
} SerialCommand;


typedef void (*CommandFunction)();

typedef struct
{
	const char *cmdName;
	CommandFunction handler;
} SerialCommandFunction;


extern SerialCommand serialCommand;

void serial_command_init(Serial* p, unsigned short cmdSize);

const char *serial_command_peek();
int serial_command_peek_int();

void serial_command_despatch();

inline unsigned char serial_command_err()
{
	return serialCommand.err != 0;
}

#endif
