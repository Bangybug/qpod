#include "Serial.h"
#include "SerialCommand.h"
#include "CommandRegistry.h"
#include <stdlib.h>
#include <string.h>


SerialCommand serialCommand;


void serial_command_init(Serial* p, unsigned short cmdSize)
{
	serialCommand.pSerial = p;
	serialCommand.cmdSize = cmdSize;
}

const char *serial_command_peek()
{
	const char *incoming = serialCommand.pSerial->incoming;

	for (unsigned short i=serialCommand.rxi; i < serialCommand.pSerial->rxi; ++i)
	{
		const unsigned char c = incoming[i];
		if (c == ' ' || c == 13 || c == 10)
		{
			serialCommand.pSerial->incoming[i] = 0;
			const char *ptr = incoming + serialCommand.rxi;
			serialCommand.rxi = i+1;

			// possible LF/CRLF (0xd/0xd 0xa)
			if (c == 13)
				++serialCommand.rxi;

			return ptr;
		}
	}

	return 0;
}


int serial_command_peek_int()
{
	const char* p = serial_command_peek();
	if (p)
		return atoi(p);
	else
	{
		serialCommand.err = 1;
		return 0;
	}
}

void serial_command_despatch()
{
	if (serialCommand.pSerial->eol)
	{
		serialCommand.rxi = 0;

		const char *cmd = serial_command_peek();

		if (cmd)
		{
			serialCommand.err = 0;

			for (unsigned short i=0; i<serialCommand.cmdSize; ++i)
			{
				if (0 == strcmp(cmd, commandRegistry[i].cmdName))
				{
					commandRegistry[i].handler();
					break;
				}
			}
		}

		serialCommand.pSerial->eol = serialCommand.pSerial->rxi = 0;
	}
}
