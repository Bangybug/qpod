#include "ServoControl.h"

#include <stm32f10x_tim.h>

ServoControl *pServocontrol;

void servocontrol_init(ServoControl *p)
{
	pServocontrol = p;

	for (int i=0; i<12; ++i)
		p->pulses[i] = 0; //SERVO_SHORTEST_PULSE;
}


void servocontrol_init_gpio()
{
	GPIO_InitTypeDef gpio;
	GPIO_StructInit(&gpio);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    // TIMER 1 - enable pa8,9,10,11 pins for timer channels
	// TIMER 2 - enable pa0,1,2,3 pins for timer channels
	// TIMER 3 - enable pa6,7
	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	gpio.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 |
			GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 |
			GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_Init(GPIOA, &gpio);

	// TIMER3 - enable pb0,1
	gpio.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_Init(GPIOB, &gpio);

}

void servocontrol_init_timers()
{
	TIM_TimeBaseInitTypeDef base_timer;
	TIM_OCInitTypeDef timer_oc;

	// Counting at 10 us, period 20 ms
	TIM_TimeBaseStructInit(&base_timer);
	base_timer.TIM_Prescaler = SystemCoreClock / 100000 - 1;
	base_timer.TIM_Period = 2000;

	TIM_OCStructInit(&timer_oc);
	timer_oc.TIM_Pulse = SERVO_SHORTEST_PULSE;
	timer_oc.TIM_OCMode = TIM_OCMode_PWM1;
	timer_oc.TIM_OutputState = TIM_OutputState_Enable;

	// TIMER 1

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	TIM_TimeBaseInit(TIM1, &base_timer);
	// channels 1-4, edge aligned pwm
	TIM_OC1Init(TIM1, &timer_oc);
	TIM_OC2Init(TIM1, &timer_oc);
	TIM_OC3Init(TIM1, &timer_oc);
	TIM_OC4Init(TIM1, &timer_oc);
	//TIM_BDTRConfig(TIM1, &bdtr);
	TIM1->BDTR = TIM_AutomaticOutput_Enable; // only want automatic output, don't use above call
	TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);
	TIM1->CNT = 0;
	TIM_Cmd(TIM1, ENABLE);
	NVIC_EnableIRQ(TIM1_UP_IRQn);

	// TIMER 2 & 3
	// manual says that timer2,3 are clocked via APB1 that has 36MHz max, so technically I've had to half
	// the TIM_Prescaler, but my scope shows 10 mS period instead of expected 20 mS, so that means
	// either APB1 is running at 72MHz as well as the system core, or I do not understand something

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	TIM_TimeBaseInit(TIM2, &base_timer);
	// channels 1-4, edge aligned pwm
	TIM_OC1Init(TIM2, &timer_oc);
	TIM_OC2Init(TIM2, &timer_oc);
	TIM_OC3Init(TIM2, &timer_oc);
	TIM_OC4Init(TIM2, &timer_oc);
	//TIM_BDTRConfig(TIM2, &bdtr);
	TIM2->BDTR = TIM_AutomaticOutput_Enable; // only want automatic output, TIM_BDTRConfig call fails for TIM2
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	TIM2->CNT = 0;
	TIM_Cmd(TIM2, ENABLE);
	NVIC_EnableIRQ(TIM2_IRQn);

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	TIM_TimeBaseInit(TIM3, &base_timer);
	// channels 1-4, edge aligned pwm
	TIM_OC1Init(TIM3, &timer_oc);
	TIM_OC2Init(TIM3, &timer_oc);
	TIM_OC3Init(TIM3, &timer_oc);
	TIM_OC4Init(TIM3, &timer_oc);
	//TIM_BDTRConfig(TIM3, &bdtr);
	TIM3->BDTR = TIM_AutomaticOutput_Enable; // only want automatic output, TIM_BDTRConfig call fails for TIM2
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	TIM3->CNT = 0;
	TIM_Cmd(TIM3, ENABLE);
	NVIC_EnableIRQ(TIM3_IRQn);

}



void TIM1_UP_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
		TIM_SetCompare1(TIM1, pServocontrol->pulses[0]);
		TIM_SetCompare2(TIM1, pServocontrol->pulses[1]);
		TIM_SetCompare3(TIM1, pServocontrol->pulses[2]);
		TIM_SetCompare4(TIM1, pServocontrol->pulses[3]);
	}
}

void TIM2_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		TIM_SetCompare1(TIM2, pServocontrol->pulses[4]);
		TIM_SetCompare2(TIM2, pServocontrol->pulses[5]);
		TIM_SetCompare3(TIM2, pServocontrol->pulses[6]);
		TIM_SetCompare4(TIM2, pServocontrol->pulses[7]);
	}
}

void TIM3_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		TIM_SetCompare1(TIM3, pServocontrol->pulses[8]);
		TIM_SetCompare2(TIM3, pServocontrol->pulses[9]);
		TIM_SetCompare3(TIM3, pServocontrol->pulses[10]);
		TIM_SetCompare4(TIM3, pServocontrol->pulses[11]);
	}
}
