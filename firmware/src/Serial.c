#include "Serial.h"
#include <stm32f10x_usart.h>
#include <string.h>

Serial *pSerial;


void serial_init(Serial *p)
{
	pSerial = p;
	pSerial->rxi = 0;
	pSerial->eol = 0;
}

void serial_init_gpio()
{
	/* USART configuration structure for USART1 */
	GPIO_InitTypeDef gpioa_init_struct;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOB, ENABLE);

	gpioa_init_struct.GPIO_Pin = GPIO_Pin_6;
	gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
	gpioa_init_struct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOB, &gpioa_init_struct);

	gpioa_init_struct.GPIO_Pin = GPIO_Pin_7;
	gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
	gpioa_init_struct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &gpioa_init_struct);

	GPIO_PinRemapConfig(GPIO_Remap_USART1, ENABLE);
}


void serial_init_usart()
{
	USART_InitTypeDef usart1_init_struct;

	USART_Cmd(USART1, ENABLE);
	usart1_init_struct.USART_BaudRate = 9600;
	usart1_init_struct.USART_WordLength = USART_WordLength_8b;
	usart1_init_struct.USART_StopBits = USART_StopBits_1;
	usart1_init_struct.USART_Parity = USART_Parity_No ;
	usart1_init_struct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	usart1_init_struct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(USART1, &usart1_init_struct);
	/* Enable RXNE interrupt */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	/* Enable USART1 global interrupt */
	NVIC_EnableIRQ(USART1_IRQn);
}


void serial_init_dma()
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	DMA_InitTypeDef DMA_InitStruct;
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&(USART1->DR);
	DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t)&pSerial->outgoing;
	DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStruct.DMA_BufferSize = TX_BUF_SIZE;
	DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStruct.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStruct.DMA_Priority = DMA_Priority_Low;
	DMA_InitStruct.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel4, &DMA_InitStruct);

	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);

	DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE);
	NVIC_EnableIRQ(DMA1_Channel4_IRQn);

	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
}

void USART1_IRQHandler(void)
{
	if (pSerial->eol != 1 && // to avoid corruption skip incoming data if there is a pending command
		USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		// buffer overrun, start from beginning
		if (pSerial->rxi == RX_BUF_SIZE)
		{
			pSerial->rxi = 0;
		}

		const char rxc = USART_ReceiveData(USART1);
		pSerial->incoming[pSerial->rxi++] = rxc;

		// command received
		if (rxc == 13)
		{
			pSerial->eol = 1;
		}
	}
}

const char *serial_as_string()
{
	if (pSerial->eol && pSerial->rxi > 0)
	{
		pSerial->incoming[pSerial->rxi - 1] = 0;
		return pSerial->incoming;
	}
	return "";
}


void DMA1_Channel4_IRQHandler(void)
{
    DMA_ClearITPendingBit(DMA1_IT_TC4);
    DMA_Cmd(DMA1_Channel4, DISABLE);
}


void serial_send_dma(const char *buf)
{
    strcpy(pSerial->outgoing, buf);
    DMA_Cmd(DMA1_Channel4, DISABLE);
    DMA1_Channel4->CNDTR = strlen(buf);
    DMA_Cmd(DMA1_Channel4, ENABLE);
}


