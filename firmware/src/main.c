#include "ServoControl.h"
#include "Serial.h"
#include "CommandRegistry.h"
#include "SerialCommand.h"


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"


const SerialCommandFunction commandRegistry[] = {
	{ "p", command_servo_set_pulse }
};

ServoControl servo;
Serial serial;


int main(int argc, char* argv[])
{
	servocontrol_init(&servo);
	servocontrol_init_gpio();
	servocontrol_init_timers();

	serial_init(&serial);
	serial_init_gpio();
	serial_init_usart();
	serial_init_dma();

	serial_command_init(&serial, sizeof(commandRegistry)/sizeof(SerialCommandFunction));

	serial_send_dma("OK\r\n");

	while(1)
	{
		serial_command_despatch();
	}
}



void command_servo_set_pulse()
{
	const int servoId = serial_command_peek_int();
	const int servoPulse = serial_command_peek_int();
	if (!serial_command_err())
	{
		servo.pulses[servoId] = servoPulse;
	}
}

// Power consumption test: continuously rotates all servo motors
// To use, also configure in main: SysTick_Config(SystemCoreClock / SERVO_LONGEST_PULSE);
//
//void SysTick_Handler(void)
//{
//	static int direction = 1;
//	static int pulse = SERVO_SHORTEST_PULSE;
//
//	pulse += direction;
//	if (pulse >= SERVO_LONGEST_PULSE)
//	{
//		direction = -direction;
//		pulse = SERVO_LONGEST_PULSE;
//	}
//	else if (pulse < SERVO_SHORTEST_PULSE )
//	{
//		direction = -direction;
//		pulse = SERVO_SHORTEST_PULSE;
//	}
//
//	for (int i=0; i<12; ++i)
//	{
//		servo.pulses[i] = pulse;
//	}
//}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
