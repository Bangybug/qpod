# Python Blender script used for gait prototyping
# The result of the script is the keyframe animation sequence created by anim* methods in CWalker class
# Physics simulation cannot be easily implemented in Blender, because they have removed Blender game engine in 2.8
# @author Dmitry Pryadkin, drpadawan@ya.ru, https://vk.com/bug_dog

import bpy
import colorsys
from math import sqrt, pi, sin, cos, ceil, atan, acos, sqrt, pow, radians, degrees
from random import TWOPI

#
# Animation variables.
#

FPS = 30

#
# Robot properties
#

LEG_RFRONT = 0
LEG_LFRONT = 1
LEG_RREAR = 2
LEG_LREAR = 3

COXA = 0
FEMUR = 1
TIBIA = 2

SG90_SERVO_SPEED = radians(60) / 0.15  

# CModel class defines 3d model of the robot, model parts are linked in Blender using parent-child relationship
# each leg has 3 parts, if counted from the base they are named coxa (Leg), femur, tibia
class CModel:
    legRFront = bpy.data.objects["LegRFront"]
    femurRFront = bpy.data.objects["FemurRFront"]
    tibiaRFront = bpy.data.objects["TibiaRFront"]

    legLFront = bpy.data.objects["LegLFront"]
    femurLFront = bpy.data.objects["FemurLFront"]
    tibiaLFront = bpy.data.objects["TibiaLFront"]

    legRRear = bpy.data.objects["LegRRear"]
    femurRRear = bpy.data.objects["FemurRRear"]
    tibiaRRear = bpy.data.objects["TibiaRRear"]

    legLRear = bpy.data.objects["LegLRear"]
    femurLRear = bpy.data.objects["FemurLRear"]
    tibiaLRear = bpy.data.objects["TibiaLRear"]

    def __init__(self):
        self.legs = [ [self.legRFront, self.femurRFront, self.tibiaRFront], [self.legLFront, self.femurLFront, self.tibiaLFront],  [self.legRRear, self.femurRRear, self.tibiaRRear],  [self.legLRear, self.femurLRear, self.tibiaLRear]]

    def setLegPart(self, leg, part, angle): 
        self.legs[leg][part].rotation_euler = (angle, 0.0, 0.0) if part > 0 else (0.0, 0.0, angle)
        self.legs[leg][part].keyframe_insert(data_path='rotation_euler')


# CDimensions class defines basic dimensions of the robot in millimeters
# dimensions can be measured either in Blender or on real 3d-printed objects, lengths measured between servo shafts (joints)
class CDimensions: 
    coxa = 27.5 
    femur = 55
    tibia = 77.1 # distance from servo shaft to the tip that touches the ground
    sideLength = 71  # distance between servo shafts attached to the base
    zFemur = 28 # distnace from femur shaft to the bottom base level     

# CLimits class defines limits, in degrees, for the legs joints to avoid some self-collisions with other joints of the same leg, or with the base
# note: - self collisions may still occur, even if all individual joints stay within the limits
# - limits are given for the front right leg, other legs limits need to be symmetrically adjusted (change signs)
# - the first limit maps to servo zero orientation (e.g. -37 degrees maps to servo zero)
class CLimits: 
    coxa = [radians(-37), radians(90)]
    femur = [radians(-36), radians(120)]
    tibia = [radians(150), radians(30)]

# CLegIK takes desired leg tip position (x,y,z) in millimeters as input and returns angles per each joint of the leg.
# Let's agree on coordinate system axes:  
# 1) Global coordinate system: X - forward, Z - up, Y - left
# 2) Leg coordinate system is the same as global, with the origin at coxa shaft; that means right and left legs are symmetrical in terms of their Y-positions
# 3) Agree on leg initial zero pose where it's all streightened and points sideways
class CLegIK:
    def __init__(self, dimensions):
        self.dimensions = dimensions
        self.legAngles = [ [0,0,0], [0,0,0], [0,0,0], [0,0,0] ]
        self.legPos = [ [0,0,0], [0,0,0], [0,0,0], [0,0,0] ]

    def updateAllLegPos(self):
        for i in range(4):
            self.calculateLegPos(i)

    def calculateLegPos(self, leg):

        legAngles = self.legAngles[leg]

        print("{0} {1} {2}".format(degrees(legAngles[0]), degrees(legAngles[1]), degrees(legAngles[2])))

        alpha = legAngles[0]
        beta =  legAngles[1]
        gamma = legAngles[2]

        if leg == LEG_RFRONT:
            alpha = -pi/2 + legAngles[0]
        if leg == LEG_LFRONT:
            alpha = pi/2 + legAngles[0]
        if leg == LEG_RREAR:
            alpha = -pi/2 + legAngles[0]
        if leg == LEG_LREAR:
            alpha = pi/2 + legAngles[0]


        # alpha = pi/2-legAngles[0]
        # beta = -legAngles[1]
        # gamma = pi/2-legAngles[1]-legAngles[2]

        # if leg == LEG_RFRONT:
        #     alpha = pi/2 - legAngles[0]
        #     beta = -legAngles[1]
        #     gamma = pi/2-legAngles[1]-legAngles[2]


        
        legLengthXY = self.dimensions.coxa + self.dimensions.femur * cos(beta) + self.dimensions.tibia * sin(gamma - beta)
        
        x = legLengthXY * cos(alpha)
        y = legLengthXY * sin(alpha)
        z = self.dimensions.zFemur + self.dimensions.femur * sin(beta) - self.dimensions.tibia * cos(gamma - beta)

        self.legPos[leg] = [x,y,z]


    def calculateLegAngles(self, leg, x,y,z):
        ret = self.legAngles[leg][:]

        # if y is zero, the leg is rotated by pi/2 either one side or another, collinear with the body (indeterminate) 
        if y == 0:
            ret[0] = pi/2
        # if x is zero, the leg is either at zero angle or rotated by pi angle (indeterminate), but as agreed, zero angle sets leg away from the body, which is still better 
        if x == 0:
            ret[0] = 0
        if x != 0 and y != 0:
            ret[0] = -atan(x/y)

        z += self.dimensions.zFemur

        # let there be 4 points, OABC, where O is zero, OA is the coxa, ABC is the triangle formed by last two leg joints (tibia-femur), all 4 points are on a single plane 
        # as follows from leg structure
        OCxy = sqrt(pow(x,2) + pow(y,2)) # projection of OC onto xy plane
        AC = sqrt( pow(z,2) + pow(OCxy-self.dimensions.coxa, 2) )

        ret[1] = acos(z/AC) + acos( (pow(self.dimensions.tibia,2) - pow(self.dimensions.femur,2) - pow(AC,2)) / (-2*self.dimensions.femur*AC) )
        ret[2] = acos( (pow(AC,2) - pow(self.dimensions.tibia,2) - pow(self.dimensions.femur,2)) / (-2*self.dimensions.tibia*self.dimensions.femur) )

        # adjust angle zero value to match initial pose
        ret[1] -= pi/2
        ret[2] = pi - ret[2]

        # adjust as per leg symmetry
        if leg == LEG_RFRONT or leg == LEG_RREAR:
            ret[1] = -ret[1]
            
        if leg == LEG_LFRONT or leg == LEG_LREAR:
            ret[2] = -ret[2]



        return ret


# CAnimator class provides anim* method group for making animations
class CAnimator:
    def __init__(self, dimensions, limits, model, legIK):
        self.dimensions = dimensions
        self.model = model
        self.limits = limits
        self.legIK = legIK
        self.currentFrame = 1
        self.legOrigins = [ model.legs[LEG_RFRONT][COXA].location, model.legs[LEG_LFRONT][COXA].location, model.legs[LEG_RREAR][COXA].location, model.legs[LEG_LREAR][COXA].location ]
        
    def setKeyframeFromScene(self):
        bpy.context.scene.frame_set(self.currentFrame)
        for leg in range(4):
            for part in (range(3)):
                legPart = self.model.legs[leg][part]
                self.legIK.legAngles[leg][part] = legPart.rotation_euler[ 2 if part == 0 else 0 ]
                legPart.keyframe_insert(data_path='rotation_euler')
                legPart.keyframe_insert(data_path='location')
        self.currentFrame += 1


    def animLegPart(self, leg, part, angle, currentFrame, keyFrames=0):
        # instantly move
        if keyFrames == 0:
            bpy.context.scene.frame_set(currentFrame)
            self.legIK.legAngles[leg][part] = angle
            self.model.setLegPart(leg, part, angle)
            return currentFrame+1
        # move by keyFrames from currentFrame
        else: 
            oldAngle = self.legIK.legAngles[leg][part]
            self.legIK.legAngles[leg][part] = angle
            delta = angle - oldAngle
            # print( "DELTA={0} oldangle={1} newangle={2}".format(delta, oldAngle, angle) )
            frameCount = ceil(abs(delta) / SG90_SERVO_SPEED * FPS)
            endFrame = currentFrame + frameCount
            invKeyframeCount = 1.0 / keyFrames
            framesPerKey = frameCount * invKeyframeCount
            for f in range(0, keyFrames):
                currentFrame += framesPerKey
                animProgress = (f+1) * invKeyframeCount
                bpy.context.scene.frame_set(currentFrame)
                self.model.setLegPart(leg,part, oldAngle+delta*animProgress)
            return endFrame


    def animServoZeroPose(self, keyFrames=0): 
        currentFrame = self.currentFrame
        nextFrame = self.animLegPart(LEG_RFRONT, COXA, self.limits.coxa[0], currentFrame, keyFrames)
        nextFrame = max(nextFrame, self.animLegPart(LEG_LFRONT, COXA, -self.limits.coxa[1], currentFrame, keyFrames))
        nextFrame = max(nextFrame, self.animLegPart(LEG_RREAR, COXA,  -self.limits.coxa[1], currentFrame, keyFrames))
        nextFrame = max(nextFrame, self.animLegPart(LEG_LREAR, COXA,  self.limits.coxa[0], currentFrame, keyFrames))

        nextFrame = max(nextFrame, self.animLegPart(LEG_RFRONT, FEMUR, -self.limits.femur[1], currentFrame, keyFrames))
        nextFrame = max(nextFrame, self.animLegPart(LEG_LFRONT, FEMUR, self.limits.femur[0], currentFrame, keyFrames))
        nextFrame = max(nextFrame, self.animLegPart(LEG_RREAR, FEMUR,  -self.limits.femur[0], currentFrame, keyFrames))
        nextFrame = max(nextFrame, self.animLegPart(LEG_LREAR, FEMUR,  self.limits.femur[1], currentFrame, keyFrames))
        
        nextFrame = max(nextFrame, self.animLegPart(LEG_RFRONT, TIBIA, self.limits.tibia[0], currentFrame, keyFrames))
        nextFrame = max(nextFrame, self.animLegPart(LEG_LFRONT, TIBIA, -self.limits.tibia[1], currentFrame, keyFrames))
        nextFrame = max(nextFrame, self.animLegPart(LEG_RREAR, TIBIA,  self.limits.tibia[1], currentFrame, keyFrames))
        nextFrame = max(nextFrame, self.animLegPart(LEG_LREAR, TIBIA,  -self.limits.tibia[0], currentFrame, keyFrames))
        self.currentFrame = nextFrame
        return nextFrame


    def animLegTipIK(self, leg, legAngles, currentFrame, keyFrames=0):
        nextFrame = self.animLegPart(leg, COXA, legAngles[0], currentFrame, keyFrames)
        nextFrame = max(nextFrame, self.animLegPart(leg, FEMUR, legAngles[1], currentFrame, keyFrames))
        nextFrame = max(nextFrame, self.animLegPart(leg, TIBIA, legAngles[2], currentFrame, keyFrames))
        #print("angles {0} {1} {2}".format(degrees(legAngles[0]), degrees(legAngles[1]), degrees(legAngles[2])));
        return nextFrame


    def animStand(self, keyFrames=0):
        currentFrame = self.currentFrame
        
        # multiply by 1000 to get to the millimeter scale
        legAngles = self.legIK.calculateLegAngles(LEG_RFRONT, 100-self.legOrigins[LEG_RFRONT][0]*1000, -70-self.legOrigins[LEG_RFRONT][1]*1000, 0)
        nextFrame = self.animLegTipIK(LEG_RFRONT, legAngles, currentFrame, keyFrames)
        #self.debugCalc(LEG_RFRONT, legAngles)
        legAngles = self.legIK.calculateLegAngles(LEG_LFRONT, 100-self.legOrigins[LEG_LFRONT][0]*1000, 70-self.legOrigins[LEG_LFRONT][1]*1000, 0)
        nextFrame = self.animLegTipIK(LEG_LFRONT, legAngles, currentFrame, keyFrames)
        #self.debugCalc(LEG_LFRONT, legAngles)
        legAngles = self.legIK.calculateLegAngles(LEG_RREAR, -100-self.legOrigins[LEG_RREAR][0]*1000, -70-self.legOrigins[LEG_RREAR][1]*1000, 0)
        nextFrame = self.animLegTipIK(LEG_RREAR, legAngles, currentFrame, keyFrames)
        #self.debugCalc(LEG_RREAR, legAngles)
        legAngles = self.legIK.calculateLegAngles(LEG_LREAR, -100-self.legOrigins[LEG_LREAR][0]*1000, 70-self.legOrigins[LEG_LREAR][1]*1000, 0)
        nextFrame = self.animLegTipIK(LEG_LREAR, legAngles, currentFrame, keyFrames)
        #self.debugCalc(LEG_LREAR, legAngles)

        self.currentFrame = nextFrame
        return nextFrame

    def debugCalc(self, leg, legAngles):
        alpha = pi/2-legAngles[0]
        beta = -legAngles[1]
        gamma = pi/2-legAngles[1]-legAngles[2]
        
        legLengthXY = self.dimensions.coxa + self.dimensions.femur * cos(beta) + self.dimensions.tibia * sin(gamma)
        
        x = legLengthXY * cos(alpha)
        y = legLengthXY * sin(alpha)
        z = self.dimensions.zFemur + self.dimensions.femur * sin(beta) - self.dimensions.tibia * cos(gamma)

        print("Calc check: x={0} y={1} z={2} legXy={3}".format(x,y,z, legLengthXY))


# this gait I adapted from another opensource project - Remote control crawling robot
# also known as https://www.instructables.com/id/DIY-Spider-RobotQuad-robot-Quadruped/
class CGaitArduinoSpider:
    def __init__(self, animator):
        # offset for a leg in walk-ready position (lesser - the closer the leg tip to the base)
        xOffset = 100 
        yOffset = 70 
        # step length
        stepSize = 40
        # height at which the robot elevates during step       
        zLegLift = -30
        # if in sync, all pending moves have to be accomplished before further moves can start
        sync = True
        noSync = False

        # speed given in mm per second
        legSpeed = 8
        bodySpeed= 3        

        self.animator = animator
        self.stepForward = [ [
            LEG_RFRONT, legSpeed, sync, xOffset, -yOffset, zLegLift, 
            LEG_RFRONT, legSpeed, sync, xOffset + 2*stepSize, -yOffset, zLegLift, 
            LEG_RFRONT, legSpeed, sync, xOffset + 2*stepSize, -yOffset, 0,
            LEG_LFRONT, bodySpeed, noSync, xOffset, yOffset, 0, 
            LEG_LREAR,  bodySpeed, noSync, -xOffset + 2*stepSize, yOffset, 0, 
            LEG_RREAR,  bodySpeed, noSync, -xOffset + 2*stepSize, yOffset, 0] ]



def debugAnglesToPos(): 
    leg = LEG_RFRONT
    currentFrame = animator.currentFrame
    legAngles = [radians(61.7), radians(-62), radians(138)]
    nextFrame = animator.animLegPart(leg, COXA, legAngles[0], currentFrame, 1)
    nextFrame = max(nextFrame, animator.animLegPart(leg, FEMUR, legAngles[1], currentFrame, 1))
    nextFrame = max(nextFrame, animator.animLegPart(leg, TIBIA, legAngles[2], currentFrame, 1))
    animator.debugCalc(leg, legAngles)
    animator.currentFrame = currentFrame

print("\nScript started")

dimensions = CDimensions()
model = CModel()
limits = CLimits()
legIK = CLegIK(dimensions)
animator = CAnimator(dimensions, limits, model, legIK)
gait = CGaitArduinoSpider(animator)

animator.setKeyframeFromScene()
#animator.animServoZeroPose(1)
animator.animStand(1)

#debugAnglesToPos()

legIK.updateAllLegPos()
print(legIK.legPos)


bpy.context.scene.frame_end = animator.currentFrame
